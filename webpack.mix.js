const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('public/vendor/jquery-treegrid/js/jquery.treegrid.min.js', 'public/js')
    .js('resources/js/custom.js', 'public/js')
    .postCss('public/vendor/jquery-treegrid/css/jquery.treegrid.css', 'public/css', [
        //
    ]);
