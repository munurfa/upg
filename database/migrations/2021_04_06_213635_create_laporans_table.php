<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporans', function (Blueprint $table) {
            $table->id();
            $table->char('noreg', 30)->unique();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('jenis_penerimaan_id')->nullable();
            $table->unsignedBigInteger('jenis_peristiwa_id')->nullable();
            $table->enum('status', ['terkirim', 'diterima', 'ditolak', 'spam']);
            $table->enum('jenis_laporan', ['penerimaan', 'penolakan']);
            $table->enum('kerahasiaan', ['ditembuskan', 'pribadi']);
            $table->float('nominal', 20, 2);
            $table->float('eq', 20, 2);
            $table->char('mata_uang', 5)->nullable();
            $table->text('uraian');
            $table->text('alamat_penerimaan');
            $table->date('tanggal_penerimaan');
            $table->string('nama_pemberi');
            $table->string('pekerjaan_pemberi')->nullable();
            $table->text('alamat_pemberi')->nullable();
            $table->string('email_pemberi')->nullable();
            $table->char('nohp_pemberi', 30)->nullable();
            $table->string('hubungan_pemberi');
            $table->text('alasan');
            $table->text('kronologi');
            $table->text('dokumen')->nullable();
            $table->text('diserahkan')->nullable();
            $table->text('keterangan')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporans');
    }
}
