<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPelaporColumnForLaporanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laporans', function (Blueprint $table)
        {
            $table->string('nama_pelapor')->nullable();
            $table->string('pekerjaan_pelapor')->nullable();
            $table->text('alamat_pelapor')->nullable();
            $table->string('email_pelapor')->nullable();
            $table->char('nohp_pelapor', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laporans', function (Blueprint $table)
        {
            $table->dropColumn('nama_pelapor');
            $table->dropColumn('pekerjaan_pelapor');
            $table->dropColumn('alamat_pelapor');
            $table->dropColumn('email_pelapor');
            $table->dropColumn('nohp_pelapor');
        });
    }
}
