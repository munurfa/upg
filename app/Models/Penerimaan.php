<?php

namespace App\Models;

use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Screen\AsSource;

class Penerimaan extends Model
{
    use HasFactory, AsSource, SoftDeletes, Loggable;

    protected $table = 'jenis_penerimaans';

    protected $fillable = [
        'name',
        'keterangan'
    ];

    protected $allowedSorts = [
        'name',
        'updated_at'
    ];

}
