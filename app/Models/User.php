<?php

namespace App\Models;

use App\Orchid\Presenters\UserPresenter;
use DateTimeInterface;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Platform\Models\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{

    use Attachable, Loggable, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'permissions',
        'avatar',
        'jabatan',
        'satker_id',
        'nid',
        'tempat_lahir',
        'tanggal_lahir',
        'phone',
        'address'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'permissions',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'permissions'          => 'array',
        'email_verified_at'    => 'datetime',
        'tanggal_lahir'        => 'datetime:Y-m-d',
    ];

    /**
     * The attributes for which you can use filters in url.
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'name',
        'email',
        'permissions',
    ];

    /**
     * The attributes for which can use sort in url.
     *
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'name',
        'email',
        'updated_at',
        'created_at',
    ];

    public function getTanggalLahirAttribute($value)
    {
        $time = strtotime($value);
        $value = date('d-m-Y', $time);

        return $value;
    }

    public function setTanggalLahirAttribute($value)
    {
        $time = strtotime($value);
        $value = date('Y-m-d', $time);

        $this->attributes['tanggal_lahir'] = $value;
    }

    public function avatars()
    {
        return $this->hasOne(Attachment::class, 'id', 'avatar')->withDefault();
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class)->withDefault();
    }

}
