<?php

namespace App\Models;

use App\Models\User;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Screen\AsSource;

class Satker extends Model
{
    use HasFactory, AsSource, SoftDeletes, Loggable;

    protected $fillable = [
        'name',
        'keterangan',
        'parent_id'
    ];

    protected $allowedSorts = [
        'name',
        'updated_at'
    ];

    public function parent(){
        return $this->belongsTo(Satker::class, 'parent_id');
    }

    public function children(){
        return $this->hasMany(Satker::class, 'parent_id');
    }

    public function getTaxonomy($taxID){
        $item = Satker::where("id", $taxID)->first();

        return $item;
    }

    static  public function getKids($parent){
        $kids = Satker::where('parent_id',$parent)->get();

        return $kids;
    }
  
    static public function sortedTerms($taxonomy, $parentID = null, $level=0, $list=[])
    {
        $pointer = '';

        for ($i=0; $i < $level; $i++) {
            $pointer = $pointer.'-';
        }

        if($level==0){
            $terms = Satker::whereNull('parent_id')->orWhere("parent_id", 0)->with('children')->get();
        }else{
            $terms = Satker::where("parent_id", $parentID)->with('children')->get();
        }
        

        foreach ($terms as $k => $t) {
          $t->level   = $level;
          $t->pointer = $pointer;
          $list[] = $t;
          $list   = self::sortedTerms($taxonomy, $t->id, $level+1, $list);
        }
        
        return $list;
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }
}
