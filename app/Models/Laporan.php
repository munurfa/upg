<?php

namespace App\Models;

use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Laporan extends Model
{
    use HasFactory, AsSource, SoftDeletes, Attachable, Filterable, Loggable;

    protected $fillable = [
        'noreg', 'user_id', 'status', 'jenis_penerimaan_id', 'jenis_peristiwa_id', 'jenis_laporan', 'kerahasiaan', 'nominal', 'eq', 'mata_uang', 'uraian', 'alamat_penerimaan', 'tanggal_penerimaan', 'nama_pemberi', 'pekerjaan_pemberi', 'alamat_pemberi', 'email_pemberi', 'nama_pelapor', 'pekerjaan_pelapor', 'alamat_pelapor', 'email_pelapor','nohp_pelapor', 'nohp_pemberi', 'hubungan_pemberi', 'alasan', 'kronologi', 'keterangan', 'dokumen', 'diserahkan', 'mata_uang'
    ];

    protected $allowedSorts = [
        'status',
        'created_at',
        'updated_at',
        'nama_pelapor',
        'user_id'
    ];

    protected $allowedFilters = [
        'noreg', 'status', 'jenis_laporan', 'created_at'
    ];


    public function getTanggalPenerimaanAttribute($value)
    {
        $time = strtotime($value);
        $value = date('d-m-Y', $time);

        return $value;
    }

    public function getNominalAttribute($value)
    {
        return number_format($value,2,'.',' ');
    }

    public function getEqAttribute($value)
    {
        return number_format($value,2,'.',' ');
    }

    public function setTanggalPenerimaanAttribute($value)
    {
        $time = strtotime($value);
        $value = date('Y-m-d', $time);

        $this->attributes['tanggal_penerimaan'] = $value;
    }

    public function setNominalAttribute($value)
    {
        $this->attributes['nominal'] = str_replace(' ', '', $value);
    }

    public function setEqAttribute($value)
    {
        $this->attributes['eq'] = str_replace(' ', '', $value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function penerimaan()
    {
        return $this->belongsTo(Penerimaan::class, 'jenis_penerimaan_id', 'id');
    }

    public function peristiwa()
    {
        return $this->belongsTo(Peristiwa::class, 'jenis_peristiwa_id', 'id');
    }
}
