<?php

namespace App\Notifications;

use App\Models\Laporan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Orchid\Platform\Notifications\DashboardChannel;
use Orchid\Platform\Notifications\DashboardMessage;
use Orchid\Support\Color;

class LaporanStatus extends Notification
{
    use Queueable;

    public $laporan;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Laporan $laporan)
    {
        $this->laporan = $laporan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [DashboardChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toDashboard($notifiable)
    {
        return (new DashboardMessage)
            ->title('<b>#'.$this->laporan->noreg.'</b> Perubahan status laporan menjadi <b>'.strtoupper($this->laporan->status).'</b>')
            ->message('Laporan telah berubah status. Mohon segera direview')
            ->type(Color::INFO())
            ->action(route('platform.laporan.list').'?filter%5Bnoreg%5D='.$this->laporan->noreg);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
