<?php

namespace App\Orchid\Screens\Satker;

use App\Models\Peristiwa;
use App\Models\Satker;
use Illuminate\Http\Request;
use Orchid\Platform\Dashboard;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class SatkerListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Data Satuan Kerja';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.satker';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'tree' => Satker::where('parent_id', NULL)
                        ->orWhere('parent_id', 0)
                        ->get()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Buat Baru')
                ->icon('pencil')
                ->route('platform.satker.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::view('partials.treetable'),
        ];
    }

    public function remove(Request $request)
    {
        Peristiwa::findOrFail($request->get('id'))
            ->delete();

        Alert::info('Jenis Peristiwa telah dihapus');
    }
}
