<?php

namespace App\Orchid\Screens\Satker;

use App\Models\Satker;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class SatkerEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Tambah Satuan Kerja';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.satker';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Satker $satker): array
    {
        $this->exists = $satker->exists;

        if ($this->exists) {
            $this->name = 'Edit Satuan Kerja';
        }

        return [
            'satker' => $satker
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Buat post')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Ubah')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Hapus')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {   
        $data = [];
        $satker = Satker::all();
        $option = Satker::sortedTerms($satker, null, 0, []);

        foreach ($option as $term) {
            $data[$term->id] = $term->pointer.' '.$term->name;
        }

        return [
            Layout::rows([
                Select::make('satker.parent_id')
                    ->title('Parent')
                    ->options($data)
                    ->empty('Jadikan Parent', 0),

                Input::make('satker.name')
                    ->title('Nama')
                    ->required()
                    ->placeholder('Nama Satuan Kerja'),
                
                TextArea::make('satker.keterangan')
                    ->title('Keterangan')
                    ->rows(3)
                    ->maxlength(200)
                    ->placeholder('Keterangan Satuan Kerja'),
            ])
        ];
    }

    public function createOrUpdate(Satker $satker, Request $request)
    {
        $request->validate([
            'satker.name' => [
                'required',
            ],
        ]);

        $satker->fill($request->get('satker'))->save();

        Alert::info('Sukses membuat Satuan Kerja.');

        return redirect()->route('platform.satker.list');
    }

    public function remove(Satker $satker)
    {
        $satker->delete();

        Alert::info('Sukses menghapus Satuan Kerja.');

        return redirect()->route('platform.satker.list');
    }
}
