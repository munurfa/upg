<?php

namespace App\Orchid\Screens\Peristiwa;

use App\Models\Peristiwa;
use App\Orchid\Layouts\Peristiwa\PeristiwaListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PeristiwaListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Data Jenis Peristiwa';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.peristiwa';
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'peristiwas' => Peristiwa::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Buat Baru')
                ->icon('pencil')
                ->route('platform.peristiwa.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PeristiwaListLayout::class
        ];
    }

    public function remove(Request $request)
    {
        Peristiwa::findOrFail($request->get('id'))
            ->delete();

        Alert::info('Jenis Peristiwa telah dihapus');
    }
}
