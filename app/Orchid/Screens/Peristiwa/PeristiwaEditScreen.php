<?php

namespace App\Orchid\Screens\Peristiwa;

use App\Models\Peristiwa;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class PeristiwaEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Tambah Jenis Peristiwa';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.peristiwa';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Peristiwa $peristiwa): array
    {
        $this->exists = $peristiwa->exists;

        if($this->exists){
            $this->name = 'Ubah Peristiwa';
        }

        return [
            'peristiwa' => $peristiwa
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Simpan')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Ubah')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Hapus')
                ->icon('trash')
                ->method('remove')
                ->confirm('Data akan dihapus, mohon periksa kembali.')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('peristiwa.name')
                    ->title('Nama')
                    ->placeholder('Nama Peristiwa'),

                TextArea::make('peristiwa.keterangan')
                    ->title('Keterangan')
                    ->rows(3)
                    ->maxlength(200)
                    ->placeholder('Keterangan Peristiwa'),

            ])
        ];
    }

    public function createOrUpdate(Peristiwa $peristiwa, Request $request)
    {
        $request->validate([
            'peristiwa.name' => [
                'required',
            ],
        ]);

        $peristiwa->fill($request->get('peristiwa'))->save();

        Alert::info('Sukses membuat Jenis Peristiwa.');

        return redirect()->route('platform.peristiwa.list');
    }

    public function remove(Peristiwa $peristiwa)
    {
        $peristiwa->delete();

        Alert::info('Sukses menghapus Jenis Peristiwa.');

        return redirect()->route('platform.peristiwa.list');
    }
}
