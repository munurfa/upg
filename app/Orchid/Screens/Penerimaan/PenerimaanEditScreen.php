<?php

namespace App\Orchid\Screens\Penerimaan;

use App\Models\Penerimaan;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class PenerimaanEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Tambah Jenis Penerimaan';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.penerimaan';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Penerimaan $penerimaan): array
    {
        $this->exists = $penerimaan->exists;

        if($this->exists){
            $this->name = 'Ubah Penerimaan';
        }

        return [
            'penerimaan' => $penerimaan
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Simpan')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Ubah')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Hapus')
                ->icon('trash')
                ->method('remove')
                ->confirm('Data akan dihapus, mohon periksa kembali.')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('penerimaan.name')
                    ->title('Nama')
                    ->placeholder('Nama Penerimaan'),

                TextArea::make('penerimaan.keterangan')
                    ->title('Keterangan')
                    ->rows(3)
                    ->maxlength(200)
                    ->placeholder('Keterangan Penerimaan'),

            ])
        ];
    }

    public function createOrUpdate(Penerimaan $penerimaan, Request $request)
    {
        $request->validate([
            'penerimaan.name' => [
                'required',
            ],
        ]);

        $penerimaan->fill($request->get('penerimaan'))->save();

        Alert::info('Sukses membuat Jenis Penerimaan.');

        return redirect()->route('platform.penerimaan.list');
    }

    public function remove(Penerimaan $penerimaan)
    {
        $penerimaan->delete();

        Alert::info('Sukses menghapus Jenis Penerimaan.');

        return redirect()->route('platform.penerimaan.list');
    }
}
