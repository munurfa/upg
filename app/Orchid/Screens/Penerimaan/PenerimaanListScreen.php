<?php

namespace App\Orchid\Screens\Penerimaan;

use App\Models\Penerimaan;
use App\Orchid\Layouts\Penerimaan\PenerimaanListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PenerimaanListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Data Jenis penerimaan';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.penerimaan';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'penerimaans' => Penerimaan::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Buat Baru')
                ->icon('pencil')
                ->route('platform.penerimaan.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PenerimaanListLayout::class
        ];
    }

    public function remove(Request $request)
    {
        Penerimaan::findOrFail($request->get('id'))
            ->delete();

        Alert::info('Jenis Penerimaan telah dihapus');
    }
}
