<?php

namespace App\Orchid\Screens\Examples;

use App\Models\Laporan;
use App\Orchid\Layouts\Examples\ChartLineExample;
use App\Orchid\Layouts\Examples\ChartPieDua;
use App\Orchid\Layouts\Examples\ChartPieExample;
use App\Orchid\Layouts\Examples\MetricsExample;
use Illuminate\Support\Facades\DB;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class ExampleScreen extends Screen
{

    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Dashboard';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $data = Laporan::with(['user', 'penerimaan', 'peristiwa'])
                        ->select(DB::raw('status, jenis_laporan, created_at, MONTH(created_at) as month, DATE_FORMAT(created_at, "%m-%Y") as mounthyear'))
                        ->oldest()
                        ->get();
        
        $dataExport = [];
        $dataExport['metrics']['val1'] = 0;
        $dataExport['metrics']['val2'] = 0;
        $dataExport['charts']['values'] = [];
        $dataExport['charts']['labels'] = [];
        $dataExport['pie1']['labels'] = [];
        $dataExport['pie1']['values'] = [];
        $dataExport['pie2']['labels'] = [];
        $dataExport['pie2']['values'] = [];
        
        if ($data->count()) {
            $dataExport['metrics']['val1'] = $data->count();

            $dataExport['metrics']['val2'] = $data->where('month', Date('m'))->count();

            $lapBulan = $data->groupBy('mounthyear');
            foreach ($lapBulan as $key => $value) {
                $dataExport['charts']['values'][] = $value->count() ;
                $dataExport['charts']['labels'][] = $value->first()->created_at->format('M Y');
            }
    
            $lapJenis = $data->groupBy('jenis_laporan');
            foreach ($lapJenis as $key => $value) {
                $dataExport['pie1']['values'][] = $value->count() ;
                $dataExport['pie1']['labels'][] = $value->first()->jenis_laporan;
            }
    
            $lapStatus = $data->groupBy('status');
            foreach ($lapStatus as $key => $value) {
                $dataExport['pie2']['values'][] = $value->count() ;
                $dataExport['pie2']['labels'][] = $value->first()->status;
            }
        }
        
        return [
            'charts'  => [
                [
                    'name'   => 'Laporan Gratifikasi',
                    'values' => $dataExport['charts']['values'],
                    'labels' => $dataExport['charts']['labels'],
                ],
            ],

            'pie1'  => [
                [
                    'name'   => 'Laporan Gratifikasi',
                    'values' => $dataExport['pie1']['values'],
                    'labels' => $dataExport['pie1']['labels'],
                ],
            ],

            'pie2'  => [
                [
                    'name'   => 'Laporan Gratifikasi',
                    'values' => $dataExport['pie2']['values'],
                    'labels' => $dataExport['pie2']['labels'],
                ],
            ],
            
            'metrics' => [
                ['keyValue' => $dataExport['metrics']['val1']],
                ['keyValue' => $dataExport['metrics']['val2']],
            ],
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [

            Link::make('Buat Laporan Baru')
                ->icon('pencil')
                ->route('platform.laporan.edit')

        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            MetricsExample::class,
            ChartLineExample::class,

            Layout::columns([
                ChartPieExample::class,
                ChartPieDua::class,
            ])
        ];
    }

}
