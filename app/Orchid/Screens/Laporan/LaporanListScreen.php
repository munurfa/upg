<?php

namespace App\Orchid\Screens\Laporan;

use App\Exports\LaporanExport;
use App\Models\Laporan;
use App\Notifications\LaporanStatus;
use App\Orchid\Layouts\Laporan\LaporanAlasanFormLayout;
use App\Orchid\Layouts\Laporan\LaporanFiltersLayout;
use App\Orchid\Layouts\Laporan\LaporanJenisFormLayout;
use App\Orchid\Layouts\Laporan\LaporanListLayout;
use App\Orchid\Layouts\Laporan\LaporanPelaporFormLayout;
use App\Orchid\Layouts\Laporan\LaporanPemberiFormLayout;
use App\Orchid\Layouts\Laporan\LaporanPenerimaFormLayout;
use App\Orchid\Layouts\Laporan\LaporanStatusLayout;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Modal;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class LaporanListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Data Laporan Gratifikasi';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.laporan';

    public $laporans;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $data = Laporan::with(['user', 'penerimaan', 'peristiwa'])
                ->filters()
                ->filtersApplySelection(LaporanFiltersLayout::class)
                ->latest();
        if (!Auth::user()->inRole('admin')) {
            $data = Laporan::with(['user', 'penerimaan', 'peristiwa'])->where('user_id', Auth::user()->id)->filters()->latest();
        }

        $data = (request()->has('format'))?$data->get():$data->paginate();

        return [
            'laporans' => $data,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        $req = request()->all();
        $excel = array_merge($req, array('format'=>'xls'));
        $pdf = array_merge($req, array('format'=>'pdf'));

        return [
            Link::make('Buat Baru')
                ->icon('pencil')
                ->route('platform.laporan.edit'),

            Button::make('Export CSV')
                ->icon('cloud-download')
                ->method('export')
                ->rawClick(),

            DropDown::make('Export')
            ->icon('cloud-download')
            ->list([

                Button::make('EXCEL')
                    ->method('export')
                    ->icon('fa.file-excel')
                    ->parameters($excel)
                    ->rawClick(),

                Button::make('PDF')
                    ->method('showToast')
                    ->icon('fa.file-pdf')
                    ->parameters($pdf)
                    ->rawClick(),
            ]),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {

        $canAdmin = Auth::user()->hasAccess('platform.module.laporan_admin');

        return [
            LaporanFiltersLayout::class,
            LaporanListLayout::class,

            Layout::modal('review', [
                Layout::columns([
                    Layout::blank([

                        LaporanJenisFormLayout::class,
                        LaporanPenerimaFormLayout::class

                    ]),

                    Layout::blank([
                        ($canAdmin)?LaporanPelaporFormLayout::class:[],
                        LaporanPemberiFormLayout::class,

                        Layout::rows([
                            Upload::make('laporan.attachment')
                                ->title('Dokumen Unggah')
                                ->acceptedFiles('image/*,application/pdf,.zip,.rar,.doc,.docx,.xls,.xlsx')

                        ])->title('Unggah Dokumen'),
                    ])
                ]),

                LaporanAlasanFormLayout::class,

                LaporanStatusLayout::class

            ])
                ->async('asyncGetLaporan')
                ->withoutApplyButton()
                ->size(Modal::SIZE_LG),

            Layout::modal('detail', [
                Layout::columns([
                    Layout::blank([

                        LaporanJenisFormLayout::class,
                        LaporanPenerimaFormLayout::class

                    ]),

                    Layout::blank([
                        ($canAdmin)?LaporanPelaporFormLayout::class:[],
                        LaporanPemberiFormLayout::class,

                        Layout::rows([
                            Upload::make('laporan.attachment')
                                ->title('Dokumen Unggah')
                                ->acceptedFiles('image/*,application/pdf,.zip,.rar,.doc,.docx,.xls,.xlsx')

                        ])->title('Unggah Dokumen'),
                    ])
                ]),

                LaporanAlasanFormLayout::class,

            ])
                ->async('asyncGetLaporan')
                ->withoutApplyButton()
                ->size(Modal::SIZE_LG),

        ];
    }

    public function asyncGetLaporan(Laporan $laporan): array
    {
        return [
            'laporan' => $laporan,
            'is_view' => true
        ];
    }

    public function update(Laporan $laporan, $status)
    {
        if (!Auth::user()->inRole('admin')) {
            abort_unless(false, 403);
        }

        $data['status'] = $status;
        $laporan->fill($data)->save();

        $laporan->user->notify(new LaporanStatus($laporan));

        Loggable::logToDb($laporan, 'update_status');

        Alert::info('Sukses Mengubah Status Laporan Gratifikasi.');

        return redirect()->route('platform.laporan.list');
    }

    public function remove(Request $request)
    {
        Laporan::findOrFail($request->get('id'))
            ->delete();

        Alert::info('Laporan Gratifikasi telah dihapus');
    }

    public function export()
    {
        $items = $this->query();
        $data = $items['laporans'];

        $export = new LaporanExport($data);

        return Excel::download($export, 'Laporan.xls', \Maatwebsite\Excel\Excel::XLS);
    }
}
