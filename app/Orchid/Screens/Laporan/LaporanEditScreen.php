<?php

namespace App\Orchid\Screens\Laporan;

use App\Models\Laporan;
use App\Models\User;
use App\Notifications\LaporanBaru;
use App\Orchid\Layouts\Laporan\LaporanAlasanFormLayout;
use App\Orchid\Layouts\Laporan\LaporanJenisFormLayout;
use App\Orchid\Layouts\Laporan\LaporanPelaporFormLayout;
use App\Orchid\Layouts\Laporan\LaporanPemberiFormLayout;
use App\Orchid\Layouts\Laporan\LaporanPenerimaFormLayout;
use App\Orchid\Screens\Laporan\LaporanEditScreen;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Models\Role;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class LaporanEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Tambah Laporan Gratifikasi';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    public $permission = 'platform.module.laporan';

    public $exists = false;


    /**
     * Query data.
     *
     * @return array
     */
    public function query(Laporan $laporan): array
    {

        $this->exists = $laporan->exists;
        $this->laporan = $laporan;

        if($this->exists){
            $this->name = 'Ubah Laporan Gratifikasi';
        }

        if(!$this->exists){
            $laporan->tanggal_penerimaan = Date('d-m-Y');
        }

        $laporan->load(['user', 'attachment']);

        return [
            'laporan' => $laporan,
            'is_view' => false,
        ];

    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Simpan')
                ->icon('pencil')
                ->method('save')
                ->canSee(!$this->exists),

            Button::make('Ubah')
                ->icon('note')
                ->method('save')
                ->canSee($this->exists),

            Button::make('Hapus')
                ->icon('trash')
                ->method('remove')
                ->confirm('Data akan dihapus, mohon periksa kembali.')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        if (!Auth::user()->inRole('admin') && $this->laporan->user_id!= Auth::user()->id && $this->exists) {
            abort_unless(false, 403);
        }

        return [

            Layout::columns([
                Layout::blank([

                    LaporanJenisFormLayout::class,
                    LaporanPenerimaFormLayout::class

                ]),

                Layout::blank([
                    (Auth::user()->hasAccess('platform.module.laporan_admin'))?LaporanPelaporFormLayout::class:[],
                    LaporanPemberiFormLayout::class,

                    Layout::rows([
                        Upload::make('laporan.attachment')
                            ->title('Dokumen Unggah')
                            ->acceptedFiles('image/*,application/pdf,.zip,.rar,.doc,.docx,.xls,.xlsx')

                    ])->title('Unggah Dokumen'),
                ])
            ]),

            LaporanAlasanFormLayout::class

        ];
    }

    public function save(Laporan $laporan, Request $request)
    {

        if (!Auth::user()->inRole('admin') && $laporan->user_id!= Auth::user()->id && $this->exists) {
            abort_unless(false, 403);
        }

        $this->exists = $laporan->exists;

        $request->validate([
            'jenis_laporan' => ['required','in:penerimaan,penolakan'],
            'kerahasiaan' => ['required','in:ditembuskan,pribadi'],
            'laporan.jenis_penerimaan_id' => ['required'],
            'laporan.jenis_peristiwa_id' => ['required'],
            'laporan.uraian' => ['required'],
            'laporan.mata_uang' => ['required', 'string', 'max:5'],
            'laporan.nominal' => ['required'],
            'laporan.eq' => ['required'],
            'laporan.alamat_penerimaan' => ['required', 'string'],
            'laporan.tanggal_penerimaan' => ['required', 'date'],
            'laporan.nama_pemberi' => ['required', 'string', 'max:255'],
            'laporan.hubungan_pemberi' => ['required', 'string', 'max:255'],
            'laporan.alasan' => ['required', 'string'],
            'laporan.kronologi' => ['required', 'string'],
        ]);

        $data = $request->get('laporan');

        $data['kerahasiaan'] = $request->get('kerahasiaan');
        $data['jenis_laporan'] = $request->get('jenis_laporan');
        if(!$this->exists){
            $data['user_id'] = \request()->user()->id;
            $data['noreg'] = LaporanEditScreen::ticket(5);
            $data['status'] = 'terkirim';
        }

        $laporan->fill($data)->save();

        $laporan->attachment()->syncWithoutDetaching(
            $request->input('laporan.attachment', [])
        );

        if(!$this->exists){
            Loggable::logToDb($laporan, 'update_status');
            $admin = Role::find(1);
            $userAdmin = $admin->getUsers()->pluck('id');
            foreach ($userAdmin as $key => $value) {
                $user = User::find($value);
                $user->notify(new LaporanBaru($laporan));
            }
        }

        Alert::info('Sukses membuat Laporan Gratifikasi.');

        return redirect()->route('platform.laporan.list');
    }

    public function remove(Laporan $laporan)
    {
        $laporan->delete();

        Alert::info('Sukses menghapus Laporan Gratifikasi.');

        return redirect()->route('platform.laporan.list');
    }

    public function ticket($length)
	{
		$char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$char = str_shuffle($char);
		for ($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i++) {
			$rand .= $char[
				mt_rand(0, $l)];
		}
		return $rand;
	}
}
