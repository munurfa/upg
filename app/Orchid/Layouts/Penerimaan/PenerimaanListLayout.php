<?php

namespace App\Orchid\Layouts\Penerimaan;

use App\Models\Penerimaan;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PenerimaanListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'penerimaans';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('name', 'Nama'),
            TD::make('keterangan', 'Keterangan')->width('500px'),
            TD::make('updated_at', 'Terakhir diubah')
                ->render(function (Penerimaan $penerimaan) {
                    return $penerimaan->updated_at->isoFormat('DD/MM/YYYY HH:mm');
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Penerimaan $penerimaan) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([

                            Link::make(__('Edit'))
                                ->route('platform.penerimaan.edit', $penerimaan->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove')
                                ->confirm('Data akan dihapus, mohon periksa kembali.')
                                ->parameters([
                                    'id' => $penerimaan->id,
                                ]),
                        ]);
                }),
        ];
    }
}
