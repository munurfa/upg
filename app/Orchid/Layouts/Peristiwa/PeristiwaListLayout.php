<?php

namespace App\Orchid\Layouts\Peristiwa;

use App\Models\Peristiwa;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PeristiwaListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'peristiwas';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('name', 'Nama'),
            TD::make('keterangan', 'Keterangan'),
            TD::make('updated_at', 'Terakhir diubah')
                ->render(function (Peristiwa $peristiwa) {
                    return $peristiwa->updated_at->isoFormat('DD/MM/YYYY HH:mm');
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Peristiwa $peristiwa) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([

                            Link::make(__('Edit'))
                                ->route('platform.peristiwa.edit', $peristiwa->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove')
                                ->confirm('Data akan dihapus, mohon periksa kembali.')
                                ->parameters([
                                    'id' => $peristiwa->id,
                                ]),
                        ]);
                }),
        ];
    }
}
