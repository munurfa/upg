<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use App\Models\Satker;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class UserEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return array
     */
    public function fields(): array
    {
        $data = [];
        $satker = Satker::all();
        $option = Satker::sortedTerms($satker, null, 0, []);

        foreach ($option as $term) {
            $data[$term->id] = $term->pointer.' '.$term->name;
        }

        return [
            Cropper::make('user.avatar')
                ->targetId()
                ->title('Avatar')
                ->width(500)
                ->height(500),

            Input::make('user.name')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Name'))
                ->placeholder(__('Name')),

            Input::make('user.email')
                ->type('email')
                ->required()
                ->title(__('Email'))
                ->placeholder(__('Email')),

            Input::make('user.nid')
                ->type('text')
                ->max(255)
                ->title(__('NIP'))
                ->placeholder(__('Nomor Induk Pegawai')),

            Input::make('user.tempat_lahir')
                ->type('text')
                ->max(255)
                ->title(__('Tempat Lahir'))
                ->placeholder(__('Tempat Lahir')),

            DateTimer::make('user.tanggal_lahir')
                ->title('Tanggal Lahir')
                ->format('d-m-Y'),

            Select::make('user.satker_id')
                ->title('Satuan Kerja')
                ->options($data)
                ->empty('Pilih Satuan Kerja', ''),

            Input::make('user.jabatan')
                ->type('text')
                ->max(255)
                ->title(__('Jabatan'))
                ->placeholder(__('Jabatan')),

            Input::make('user.phone')
                ->mask('(99) 999-9999-99999')
                ->title('No HP/Telp (tulis kode telepon)')
                ->placeholder('No HP/Telp'),

            TextArea::make('user.address')
                ->title('Alamat Lengkap')
                ->placeholder('Uraikan alamat lengkap')
                ->rows(6),
        ];
    }
}
