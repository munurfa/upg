<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class LaporanStatusLayout extends Rows
{

    protected $title = 'Laporan';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $canStatus = Auth::user()->hasAccess('platform.module.laporan_status');
        $laporan = ($this->query->getContent('laporan'))?:false;
        
        $id = ($laporan)?$laporan->id:0;
  
        return [
            Group::make([
                Button::make('Selesai Direview')
                    ->class('btn btn-lg btn-success')
                    ->canSee($canStatus)
                    ->action('laporan_status/'.$id.'/diterima')
                    ->block(),

                Button::make('Spam')
                    ->class('btn btn-lg btn-danger')
                    ->canSee($canStatus)
                    ->action('laporan_status/'.$id.'/spam')
                    ->block(),

            ])->fullWidth(),

        ];
    }
}
