<?php

namespace App\Orchid\Layouts\Laporan;

use App\Orchid\Filters\LaporanDateRangeFilter;
use App\Orchid\Filters\LaporanStatusFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class LaporanFiltersLayout extends Selection
{
    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            LaporanStatusFilter::class,
            LaporanDateRangeFilter::class,
        ];
    }
}
