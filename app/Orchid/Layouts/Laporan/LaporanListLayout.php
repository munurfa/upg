<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Orchid\Layouts\Laporan\LaporanViewLayout;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;

class LaporanListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'laporans';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        $items = $this->query->getContent($this->target);

        return [
            TD::make('no', 'No')
                ->render(function ($value)use($items){

                    foreach($items->where('id', $value->id) as $key => $item) {
                        $no = $key + $items->firstItem();
                    }
                    return $no;
                }),
            TD::make('created_at', 'Tanggal Laporan')
                ->render(function (Laporan $laporan) {
                    return $laporan->created_at->isoFormat('DD-MM-YYYY');
                })
                ->filter(TD::FILTER_DATE)
                ->sort(),
            TD::make('noreg', 'No Register')->filter(TD::FILTER_TEXT),
            TD::make('nama_pelapor', 'Nama Pelapor')
                ->sort()
                ->render(function (Laporan $laporan) {
                    $data = $laporan->nama_pelapor;
                    if ($laporan->nama_pelapor==null) {
                        $data = $laporan->user->name;
                    }
                    return $data;
                }),
            TD::make('pekerjaan_pelapor', 'Pekerjaan/Jabatan & Unit')
                ->render(function (Laporan $laporan) {
                    $data = ($laporan->pekerjaan_pelapor)?:'Belum diisi';
                    if ($laporan->nama_pelapor==null) {
                        $satker = ($laporan->user->satker->name)?:'satker belum diatur';
                        $data = $laporan->user->jabatan.'<br>'.$satker;
                    }
                    return $data;
                }),
            TD::make('nama_pelapor', 'Dilaporkan oleh')
                ->render(function (Laporan $laporan) {
                    return $laporan->user->name;
                })
                ->sort(),
            TD::make('status', 'Status')
                ->render(function (Laporan $laporan){
                    if ($laporan->status=='terkirim') {
                        $type = 'bg-primary';
                    }elseif ($laporan->status=='diterima') {
                        $type = 'bg-success';
                    }elseif ($laporan->status=='ditolak') {
                        $type = 'bg-danger';
                    }elseif ($laporan->status=='spam') {
                        $type = 'bg-warning';
                    }
                    $badge = '<b class="badge '.$type.' mr-2 mt-1">'.Str::upper($laporan->status).'</b>';

                    return $badge;
                })
                ->sort(),

            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->render(function (Laporan $laporan) {
                    $review = '';
                    if (Auth::user()->inRole('admin')) {
                        $review = ModalToggle::make('Review')
                            ->modal('review')
                            ->modalTitle('Review Laporan Gratifikasi & Perbaharui Status #'.$laporan->noreg)
                            ->asyncParameters([
                                'laporan' => $laporan->id,
                            ])
                            ->icon('pencil')
                            ->class('btn btn-warning btn-sm');
                    }
                    return '<div class="row justify-content-around">'

                            .$review

                            .ModalToggle::make('Detail')
                                ->modal('detail')
                                ->modalTitle('Detail Laporan Gratifikasi #'.$laporan->noreg)
                                ->asyncParameters([
                                    'laporan' => $laporan->id,
                                ])
                                ->icon('eye')
                                ->class('btn btn-primary btn-sm')

                            .'</div>';

                }),
        ];
    }
}
