<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class LaporanJenisFormLayout extends Rows
{

    protected $title = 'Laporan';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $is_view = ($this->query->getContent('is_view'))?:false;
        $laporan = ($this->query->getContent('laporan'))?:false;
        
        $kerahasiaan = ($laporan)?$laporan->kerahasiaan:'';
        $jenis_laporan = ($laporan)?$laporan->jenis_laporan:'';
  
        return [
            Radio::make('kerahasiaan')
                ->placeholder('Ditembuskan ke UPG')
                ->value('ditembuskan')
                ->checked($kerahasiaan == 'ditembuskan')
                ->required()
                ->disabled($is_view)
                ->title('Kerahasiaan Laporan'),

            Radio::make('kerahasiaan')
                ->placeholder('Pribadi dan Rahasia')
                ->required()
                ->value('pribadi')
                ->disabled($is_view)
                ->checked($kerahasiaan == 'pribadi'),

            Radio::make('jenis_laporan')
                ->placeholder('Penerimaan Gratifikasi')
                ->value('penerimaan')
                ->checked($jenis_laporan == 'penerimaan')
                ->required()
                ->disabled($is_view)
                ->title('Jenis Pelaporan'),

            Radio::make('jenis_laporan')
                ->placeholder('Penolakan Gratifikasi')
                ->required()
                ->value('penolakan')
                ->disabled($is_view)
                ->checked($jenis_laporan == 'penolakan'),

        ];
    }
}
