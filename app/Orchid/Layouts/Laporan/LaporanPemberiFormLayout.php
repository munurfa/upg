<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class LaporanPemberiFormLayout extends Rows
{

    protected $title = 'Data Pemberi Gratifikasi';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $is_view = ($this->query->getContent('is_view'))?:false;
  
        return [

            Label::make('laporan.nama_pemberi')
                ->title('Nama Pemberi Gratifikasi')
                ->canSee($is_view),

            Input::make('laporan.nama_pemberi')
                ->title('Nama Pemberi Gratifikasi')
                ->placeholder('Nama Lengkap')
                ->required()
                ->canSee(!$is_view),

            Label::make('laporan.pekerjaan_pemberi')
                ->title('Pekerjaan/Jabatan')
                ->canSee($is_view),

            Input::make('laporan.pekerjaan_pemberi')
                ->title('Pekerjaan/Jabatan')
                ->placeholder('Pekerjaan atau jabatan')
                ->canSee(!$is_view),

            Label::make('laporan.alamat_pemberi')
                ->title('Alamat')
                ->canSee($is_view),

            TextArea::make('laporan.alamat_pemberi')
                ->title('Alamat')
                ->placeholder('Alamat Lengkap Kantor/tempat tinggal')
                ->rows(6)
                ->canSee(!$is_view),

            Label::make('laporan.email_pemberi')
                ->title('Email')
                ->canSee($is_view),

            Input::make('laporan.email_pemberi')
                ->title('Email')
                ->placeholder('Email Pemberi Gratifikasi')
                ->type('email')
                ->canSee(!$is_view),

            Label::make('laporan.nohp_pemberi')
                ->title('No HP/Telp (tulis kode telepon)')
                ->canSee($is_view),

            Input::make('laporan.nohp_pemberi')
                ->mask('(99) 999-9999-99999')
                ->title('No HP/Telp (tulis kode telepon)')
                ->placeholder('No HP/Telp Pemberi Gratifikasi')
                ->canSee(!$is_view),

            Label::make('laporan.hubungan_pemberi')
                ->title('Hubungan dengan Pemberi Gratifikasi')
                ->canSee($is_view),

            Input::make('laporan.hubungan_pemberi')
                ->title('Hubungan dengan Pemberi Gratifikasi')
                ->placeholder('Hubungan kerja atau saudara')
                ->required()
                ->canSee(!$is_view),

        ];
    }
}
