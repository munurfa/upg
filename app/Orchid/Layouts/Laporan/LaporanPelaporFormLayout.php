<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class LaporanPelaporFormLayout extends Rows
{

    protected $title = 'Data Pelapor Gratifikasi';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $is_view = ($this->query->getContent('is_view'))?:false;
        $laporan = ($this->query->getContent('laporan'))?:false;
        $permission = Auth::user()->hasAccess('platform.module.laporan_admin');

        $nama_pelapor = ($laporan)?$laporan->nama_pelapor:'';
        $pekerjaan_pelapor = ($laporan)?$laporan->pekerjaan_pelapor:'';
        $alamat_pelapor = ($laporan)?$laporan->alamat_pelapor:'';
        $email_pelapor = ($laporan)?$laporan->email_pelapor:'';
        $nohp_pelapor = ($laporan)?$laporan->nohp_pelapor:'';
        
        if ($nama_pelapor==null && $permission && $laporan) {
            if ($laporan->exist) {
                $nama_pelapor = ($laporan)?$laporan->user->name:'Nama Belum Diatur';
                $pekerjaan_pelapor = ($laporan)?$laporan->user->satker->name:'Pekerjaan Belum Diatur';
                $alamat_pelapor = ($laporan)?$laporan->user->address:'Alamat Belum Diatur';
                $email_pelapor = ($laporan)?$laporan->user->email:'Email Belum Diatur';
                $nohp_pelapor = ($laporan)?$laporan->user->phone:'No HP Belum Diatur';
            }
        } 
       
  
        return [

            Label::make('laporan.nama_pelapor')
                ->title('Nama Pelapor Gratifikasi')
                ->value($nama_pelapor)
                ->canSee($is_view),

            Input::make('laporan.nama_pelapor')
                ->title('Nama Pemberi Gratifikasi')
                ->placeholder('Nama Lengkap')
                ->required()
                ->canSee(!$is_view && $permission),

            Label::make('laporan.pekerjaan_pelapor')
                ->title('Pekerjaan/Jabatan')
                ->value($pekerjaan_pelapor)
                ->canSee($is_view),

            Input::make('laporan.pekerjaan_pelapor')
                ->title('Pekerjaan/Jabatan')
                ->placeholder('Pekerjaan atau jabatan')
                ->canSee(!$is_view && $permission),

            Label::make('laporan.alamat_pelapor')
                ->title('Alamat')
                ->value($alamat_pelapor)
                ->canSee($is_view),

            TextArea::make('laporan.alamat_pelapor')
                ->title('Alamat')
                ->placeholder('Alamat Lengkap Kantor/tempat tinggal')
                ->rows(6)
                ->canSee(!$is_view && $permission),

            Label::make('laporan.email_pelapor')
                ->title('Email')
                ->value($email_pelapor)
                ->canSee($is_view),

            Input::make('laporan.email_pelapor')
                ->title('Email')
                ->placeholder('Email Pelapor Gratifikasi')
                ->type('email')
                ->canSee(!$is_view && $permission),

            Label::make('laporan.nohp_pelapor')
                ->title('No HP/Telp (tulis kode telepon)')
                ->value($nohp_pelapor)
                ->canSee($is_view),

            Input::make('laporan.nohp_pelapor')
                ->mask('(99) 999-9999-99999')
                ->title('No HP/Telp (tulis kode telepon)')
                ->placeholder('No HP/Telp Pelapor Gratifikasi')
                ->canSee(!$is_view && $permission),

        ];
    }
}
