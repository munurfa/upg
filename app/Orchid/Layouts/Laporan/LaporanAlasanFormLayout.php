<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class LaporanAlasanFormLayout extends Rows
{

    protected $title = 'Alasan Dan Kronologi';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $is_view = ($this->query->getContent('is_view'))?:false;
  
        return [

            Label::make('laporan.alasan')
                ->title('Alasan Pemberian')
                ->canSee($is_view),

            TextArea::make('laporan.alasan')
                ->title('Alasan Pemberian')
                ->required()
                ->placeholder('Uraikan alasan pemberian')
                ->rows(6)
                ->canSee(!$is_view),

            Label::make('laporan.kronologi')
                ->title('Kronologi Penerimaan')
                ->canSee($is_view),

            TextArea::make('laporan.kronologi')
                ->title('Kronologi Penerimaan')
                ->required()
                ->placeholder('Uraikan kronologin penerimaan')
                ->rows(6)
                ->canSee(!$is_view),

            Label::make('laporan.dokumen')
                ->title('Dokumen Lainnya')
                ->canSee($is_view),

            TextArea::make('Dokumen Lainnya')
                ->title('Dokumen Lainnya')
                ->placeholder('ketikkan dokumen-dokumen lain di sini jika ada')
                ->rows(6)
                ->canSee(!$is_view),

            Label::make('laporan.diserahkan')
                ->title('Uang / Barang yang diserahkan')
                ->canSee($is_view),

            TextArea::make('laporan.diserahkan')
                ->title('Uang / Barang yang diserahkan')
                ->placeholder('ketikkan dengan detail uang / barang yang dititipkan ke KPK jika ada')
                ->rows(6)
                ->canSee(!$is_view),

            Label::make('laporan.keterangan')
                ->title('Catatan Tambahan (bila perlu)')
                ->canSee($is_view),

            TextArea::make('laporan.keterangan')
                ->title('Catatan Tambahan (bila perlu)')
                ->placeholder('Catatan Tambahan (bila perlu)')
                ->rows(6)
                ->canSee(!$is_view),

        ];
    }
}
