<?php

namespace App\Orchid\Layouts\Laporan;

use App\Models\Laporan;
use App\Models\Penerimaan;
use App\Models\Peristiwa;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Label;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class LaporanPenerimaFormLayout extends Rows
{

    protected $title = 'Data Penerimaan Gratifikasi';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $is_view = ($this->query->getContent('is_view'))?:false;

        return [
            Label::make('laporan.penerimaan.name')
                ->title('Jenis Penerimaan')
                ->canSee($is_view),

            Relation::make('laporan.jenis_penerimaan_id')
                ->title('Jenis Penerimaan')
                ->required()
                ->fromModel(Penerimaan::class, 'name')
                ->canSee(!$is_view),

            Label::make('laporan.uraian')
                ->title('Uraian')
                ->canSee($is_view),

            TextArea::make('laporan.uraian')
                ->title('Uraian')
                ->placeholder('Uraian atas jenis penerimaan')
                ->required()
                ->rows(6)
                ->canSee(!$is_view),


            Label::make('laporan.mata_uang')
                ->title('Kode Mata Uang')
                ->canSee($is_view),

            Select::make('laporan.mata_uang')
                ->title('Kode Mata Uang')
                ->required()
                ->options(config('setup.mata_uang'))
                ->canSee(!$is_view),

            Label::make('laporan.nominal')
                ->title('Harga/Nilai Nominal/Taksiran')
                ->canSee($is_view),

            Input::make('laporan.nominal')
                ->title('Harga/Nilai Nominal/Taksiran')
                ->required()
                ->mask([
                    'alias' => 'currency',
                    'prefix' => ' ',
                    'groupSeparator' => ' ',
                    'digitsOptional' => true,
                ])
                ->help('Gunakan titik(.) untuk nilai desimal dan nilai maksimal desimal 2 digit.')
                ->canSee(!$is_view),

            Label::make('laporan.eq')
                ->title('Harga/Nilai EQ Uang / Barang')
                ->canSee($is_view),

            Input::make('laporan.eq')
                ->title('Harga/Nilai EQ Uang / Barang')
                ->required()
                ->mask([
                    'alias' => 'currency',
                    'prefix' => ' ',
                    'groupSeparator' => ' ',
                    'digitsOptional' => true,
                ])
                ->help('Gunakan titik(.) untuk nilai desimal dan nilai maksimal desimal 2 digit.')
                ->canSee(!$is_view),

            Label::make('laporan.peristiwa.name')
                ->title('Jenis Peristiwa')
                ->canSee($is_view),

            Relation::make('laporan.jenis_peristiwa_id')
                ->title('Jenis Peristiwa')
                ->required()
                ->fromModel(Peristiwa::class, 'name')
                ->canSee(!$is_view),

            Label::make('laporan.alamat_penerimaan')
                ->title('Harga/Nilai EQ Uang / Barang')
                ->canSee($is_view),

            TextArea::make('laporan.alamat_penerimaan')
                ->title('Alamat Penerimaan ')
                ->placeholder('Alamat penerimaan gratifikasi')
                ->required()
                ->rows(6)
                ->canSee(!$is_view),

            Label::make('laporan.tanggal_penerimaan')
                ->title('Tanggal Penerimaan')
                ->canSee($is_view),

            DateTimer::make('laporan.tanggal_penerimaan')
                ->title('Tanggal Penerimaan')
                ->required()
                ->format('d-m-Y')
                ->canSee(!$is_view)

        ];
    }
}
