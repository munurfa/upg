<?php

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateRange;

class LaporanDateRangeFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = [
        'tanggal'
    ];

    /**
     * @return string
     */
    public function name(): string
    {
        return 'Rentang Tanggal';
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        $tanggal = $this->request->get('tanggal');
        $startDate = Carbon::createFromFormat('Y-m-d', $tanggal['start']);
        $endDate = Carbon::createFromFormat('Y-m-d', $tanggal['end']);

        return $builder->whereBetween('created_at', [$startDate, $endDate]);
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [

            DateRange::make('tanggal')
                ->title('Rentang Tanggal')
                ->value($this->request->get('tanggal')),
        ];
    }

    public function value(): string
    {
        $tanggal = $this->request->get('tanggal');
        return 'Rentang Tanggal: '.Carbon::createFromFormat('Y-m-d', $tanggal['start'])->format('d-m-Y').' s/d '.Carbon::createFromFormat('Y-m-d', $tanggal['end'])->format('d-m-Y');
    }

}
