<?php

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Select;

class LaporanStatusFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = [
        'status',
    ];

    /**
     * @return string
     */
    public function name(): string
    {
        return 'Status';
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('status', $this->request->get('status'));
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Select::make('status')
                ->title('Status')
                ->options([
                    'terkirim' => 'Terkirim',
                    'diterima' => 'Diterima',
                    'ditolak' => 'Ditolak',
                    'spam' => 'Spam',
                ])
                ->empty()
                ->value($this->request->get('status')),
        ];
    }

    public function value(): string
    {
        return 'Status: '.$this->request->get('status');
    }

}
