<?php

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemMenu;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);
        // ...
    }

    /**
     * @return ItemMenu[]
     */
    public function registerMainMenu(): array
    {
        return [
            // Home
            ItemMenu::label('Dashboard')
                ->icon('bar-chart')
                ->route('platform.main')
                ->title('Home'),

            // Halaman Utama
            ItemMenu::label('Laporan Gratifikasi')
                ->icon('list')
                ->route('platform.laporan.list')
                ->title('Halaman Utama'),
            
            // Parameter
            ItemMenu::label('Satker')
                ->icon('organization')
                ->route('platform.satker.list')
                ->title('Parameter')
                ->permission('platform.module.satker'),

            ItemMenu::label('Jenis Penerimaan')
                ->icon('present')
                ->route('platform.penerimaan.list')
                ->permission('platform.module.penerimaan'),

            ItemMenu::label('Jenis Peristiwa')
                ->icon('briefcase')
                ->route('platform.peristiwa.list')
                ->permission('platform.module.peristiwa'),

            

        ];
    }

    /**
     * @return ItemMenu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            ItemMenu::label('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemMenu[]
     */
    public function registerSystemMenu(): array
    {
        return [
            ItemMenu::label(__('Access rights'))
                ->icon('lock')
                ->slug('Auth')
                ->active('platform.systems.*')
                ->permission('platform.systems.index')
                ->sort(1000),

            ItemMenu::label(__('Users'))
                ->place('Auth')
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->sort(1000)
                ->title(__('All registered users')),

            ItemMenu::label(__('Roles'))
                ->place('Auth')
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles')
                ->sort(1000)
                ->title(__('A Role defines a set of tasks a user assigned the role is allowed to perform.')),

            ItemMenu::label(__('Audit Trail'))
                ->place('Auth')
                ->icon('event')
                ->url('user-activity')
                ->permission('platform.systems.activity')
                ->sort(1000)
                ->title(__('All User Activity (please open in new tab or reload page).')),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('Systems'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users'))
                ->addPermission('platform.systems.activity', __('Audit Trail')),

            ItemPermission::group('Parameter')
                ->addPermission('platform.module.satker', 'Satker')
                ->addPermission('platform.module.penerimaan', 'Jenis Penerimaan')
                ->addPermission('platform.module.peristiwa', 'Jenis Peristiwa'),
            
            ItemPermission::group('Laporan Gratifikasi')
                ->addPermission('platform.module.laporan', 'CRUD')
                ->addPermission('platform.module.laporan_status', 'Ubah Status')
                ->addPermission('platform.module.laporan_admin', 'CRUD Admin')
        ];
    }

    /**
     * @return string[]
     */
    public function registerSearchModels(): array
    {
        return [
            // ...Models
            // \App\Models\User::class
        ];
    }
}
