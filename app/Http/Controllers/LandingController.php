<?php

namespace App\Http\Controllers;

use App\Models\Satker;
use App\Models\User;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Orchid\Support\Facades\Alert;

class LandingController extends Controller
{

    public function __construct() {

    }

    public function index()
    {
        $satker = [];
        $data = Satker::all();
        $option = Satker::sortedTerms($data, null, 0, []);

        foreach ($option as $term) {
            $satker[$term->id] = $term->pointer.' '.$term->name;
        }

        return view('welcome', compact('satker'));
    }

    public function register(User $user, Request $request)
    {
        $userData = $request->post();
        $userData['password'] = Hash::make($userData['password']);
        $user->fill($userData)
            ->save();

        $user->replaceRoles([2]);

        $user->sendEmailVerificationNotification();

        return redirect('admin/login')->with('status', 'Regitrasi berhasil. Periksa kotak masuk/spam email Anda untuk konfirmasi registrasi');
    }

    public function verify()
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return redirect('admin');
        }
        
        return view('auth.verify-email');
    }

    public function resend(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();

        session(['status' => 'Email verifikasi berhasil dikirim.']);

        return redirect()->route('verification.notice');
    }

    public function verification(EmailVerificationRequest $request)
    {

        $request->fulfill();

        return redirect()->route('platform.login');
    }
}
