<?php

use App\Http\Controllers\LandingController;
use Illuminate\Support\Facades\Route;

Route::get('/login', function ()
{
    return redirect('admin/login');
})->name('login');

Route::get('/email/verify', [LandingController::class, 'verify'])->middleware('auth')->name('verification.notice');

Route::post('/email/verification-notification', [LandingController::class, 'resend'])->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::get('/email/verify/{id}/{hash}', [LandingController::class, 'verification'])->middleware(['auth', 'signed'])->name('verification.verify');

Route::get('/', [LandingController::class, 'index']);
Route::post('/register', [LandingController::class, 'register'])->name('register');

