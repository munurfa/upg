@push('head')
<meta name="robots" content="noindex" />
<link
      href="{{ asset(config('setup.logo')) }}"
      sizes="any"
      type="image/png"
      id="favicon"
      rel="icon"
>
@endpush

<p class="h2 n-m font-thin v-center">
    <img src="{{ asset(config('setup.logo')) }}" alt="logo" style="width: 3em">

    <span class="m-l d-none d-sm-block">
        {{config('setup.name')}}
        <small class="v-top opacity">{{config('setup.subname')}}</small>
    </span>
</p>