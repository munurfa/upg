<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>{{ config('setup.name') }} {{ config('setup.subname') }}</title>

    <meta name="description" content="{{ config('setup.name') }}">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url" content="{{ config('app.url') }}">

    <!-- Icons -->
    <link rel="shortcut icon" href="{{ asset(config('setup.logo')) }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset(config('setup.logo')) }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset(config('setup.logo')) }}">

    <!-- Fonts and Styles -->
    @stack('css_before')
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap"
        rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{asset('vendor/landing/css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/landing/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/landing/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/landing/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/landing/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/landing/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/landing/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/landing/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" id="css-main" href="{{ asset('vendor/landing/landing.css') }}">

    @stack('css_after')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>

</head>

<body>


    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset(config('setup.logo')) }}" alt="logo" style="width: 3.5rem;margin-right:.5rem">{{ config('setup.name') }} {{ config('setup.subname') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="{{url('#panduan')}}" class="nav-link">Panduan Singkat</a></li>

                    @auth
                    <li class="nav-item cta mr-md-2"><a href="{{url('admin')}}"
                            class="nav-link">{{__('Kembali ke Dashboard')}} <i class="fa fa-long-arrow-right"></i></a>
                    </li>
                    @else

                    <li class="nav-item cta mr-md-2"><a href="{{url('admin')}}" class="nav-link">{{__('Login')}}</a>
                    </li>
                    @endauth

                </ul>
            </div>
        </div>
    </nav>
    <!-- END nav -->

    <div class="hero-wrap" style="background-image: url('{{asset('vendor/landing/images/bg_1.jpg')}}');"
        data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-start"
                data-scrollax-parent="true">
                <div class="col-lg-4 col-md-4 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                    <h1 class="mb-4 mt-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"> {{ config('setup.title') }}
                        <br><span>{{ config('setup.creator') }}</span></h1>

                </div>
                <div class="col-lg-2 col"></div>
                <div class="col-lg-6 col-md-6 mt-0 mt-md-5">
                    @auth
                    <form action="#" class="request-form ftco-animate">
                        <div class="text-center w-100">

                            <img class="img-avatar img-avatar96 rounded-circle"
                                src="{{asset('media/avatars/avatar10.jpg')}}" alt=""><br>
                            <span style="font-size: .7rem">Anda login sebagai</span>
                            <p class="font-w800 text-center my-2" style="font-weight: bold">
                                {{auth()->user()->email}}
                            </p>
                            <a href="{{url('admin')}}" class="btn btn-primary">{{__('Kembali ke Dashboard')}} <i
                                    class="fa fa-long-arrow-right"></i></a>
                        </div>
                        @else
                        <form method="POST" action="{{ route('register') }}" class="request-form ftco-animate">
                            @csrf
                            <h2>Registrasi</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div
                                        style="background-color: #fafafa;padding: 0.625rem 1.25rem;color:#000;text-transform: uppercase;font-size:.8rem">
                                        Data User
                                    </div><br>
                                    <div class="form-group">
                                        <div class="input-group ">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>

                                            <input type="text" class="form-control form-control-lg" id="nid"
                                                name="nid" placeholder="{{__('NIP')}}" required>


                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>

                                            <input type="text" class="form-control form-control-lg" id="name"
                                                name="name" placeholder="{{__('Nama Lengkap')}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>
                                            <input type="email" class="form-control form-control-lg" id="email"
                                                name="email" placeholder="Email" required>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>
                                            <input type="password" class="form-control form-control-lg" id="username"
                                                name="password" placeholder="Password" required>

                                        </div>
                                    </div>


                                </div>
                                <div class="col-md-6">
                                    <div
                                        style="background-color: #fafafa;padding: 0.625rem 1.25rem;color:#000;text-transform: uppercase;font-size:.8rem">
                                        STATUS KEDINASAN & KONTAK
                                    </div><br>
                                    <div class="form-group" id="unitsatu">
                                        <div class="input-group">

                                            <select id="unit" style="width:80% !important"
                                                class="js-select2 form-control" name="satker_id" style="width: 100%;"
                                                data-placeholder="Unit Kerja." required>
                                                <option value="0">--Unit Kerja--</option>
                                                @foreach ($satker as $key => $item)
                                                <option value="{{$key}}">{{$item}}</option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-append ml-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>
                                            <input type="text" class="form-control form-control-lg" id="jabatan"
                                                name="jabatan" placeholder="Jabatan" required>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>
                                            <input type="text" class="form-control form-control-lg" id="phone"
                                                name="phone" placeholder="{{__('No HP')}}" required>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend mr-2">
                                                <span class="input-group-text text-danger">
                                                    *
                                                </span>
                                            </div>
                                            <textarea class="form-control form-control-lg" name="address" id="alamat"
                                                cols="30" rows="5" placeholder="Alamat Rumah" required></textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Register" class="btn btn-primary py-3 px-4">
                            </div>
                            @endauth
                        </form>
                </div>
            </div>
        </div>
    </div>


    <section class="ftco-section" id="panduan">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <h2 class="mb-3">{{__('Panduan Singkat')}}</h2>
                </div>
            </div>
            <div class="ftco-schedule">
                <div class="row">
                    <div class="col-md-3 nav-link-wrap text-center text-md-right">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                            aria-orientation="vertical">
                            <a class="nav-link ftco-animate active" id="v-pills-1-tab" data-toggle="pill"
                                href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">CARA KERJA
                                APLIKASI</a>

                            <a class="nav-link ftco-animate" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2"
                                role="tab" aria-controls="v-pills-2" aria-selected="false">PENANGANAN LAPORAN</a>

                            <a class="nav-link ftco-animate" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3"
                                role="tab" aria-controls="v-pills-3" aria-selected="false">PENGGUNAAN APLIKASI</a>

                        </div>
                    </div>
                    <div class="col-md-9 tab-wrap">

                        <div class="tab-content" id="v-pills-tabContent">

                            <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel"
                                aria-labelledby="day-1-tab">
                                <div class="speaker-wrap ftco-animate d-md-flex">
                                    <i class="fa fa-address-card-o"></i>
                                    <div class="text">
                                        <h2><a href="#">Register</a></h2>
                                        <p>Pelapor mendaftarkan diri sebagai Pengguna aplikasi, untuk mendapat akses
                                            masuk aplikasi.</p>

                                    </div>
                                </div>
                                <div class="speaker-wrap ftco-animate d-md-flex">
                                    <i class="fa fa-files-o"></i>
                                    <div class="text">
                                        <h2><a href="#">Lapor</a></h2>
                                        <p>Setelah Pelapor mendaftar sebagai pengguna aplikasi, Pelapor bisa memasukkan
                                            data laporan dan dokumen pendukung, kemudian mengirim kepada Kami melalui
                                            aplikasi ini.</p>

                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="v-pills-2" role="tabpanel"
                                aria-labelledby="v-pills-day-2-tab">
                                <div class="speaker-wrap ftco-animate d-md-flex">
                                    <i class="fa fa-clock-o"></i>
                                    <div class="text">
                                        <h2><a href="#">Proses</a></h2>
                                        <p>Laporan gratifikasi yang disampaikan melalui aplikasi akan diproses oleh Kami
                                            dalam waktu 30 (tiga puluh) hari kerja sejak laporan dinyatakan lengkap dan
                                            diterima oleh Kami.</p>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-3" role="tabpanel"
                                aria-labelledby="v-pills-day-3-tab">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    {{-- <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 heading-section text-center ftco-animate">
                    <h2>Lebih Lanjut</h2>
                </div>
            </div>
            <div class="row d-flex">
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="blog-entry justify-content-end">
                        <a href="blog-single.html" class="block-20"
                            style="background-image: url('images/image_1.jpg');">
                        </a>
                        <div class="text pt-4">
                            <div class="meta mb-3">
                                <div><a href="#">July. 14, 2019</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading mt-2"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
                            <p>A small river named Duden flows by their place and supplies it with the necessary
                                regelialia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="blog-entry justify-content-end">
                        <a href="blog-single.html" class="block-20"
                            style="background-image: url('images/image_2.jpg');">
                        </a>
                        <div class="text pt-4">
                            <div class="meta mb-3">
                                <div><a href="#">July. 14, 2019</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading mt-2"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
                            <p>A small river named Duden flows by their place and supplies it with the necessary
                                regelialia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="blog-entry">
                        <a href="blog-single.html" class="block-20"
                            style="background-image: url('images/image_3.jpg');">
                        </a>
                        <div class="text pt-4">
                            <div class="meta mb-3">
                                <div><a href="#">July. 14, 2019</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading mt-2"><a href="#">Why Lead Generation is Key for Business Growth</a></h3>
                            <p>A small river named Duden flows by their place and supplies it with the necessary
                                regelialia.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    <footer class="ftco-footer ftco-bg-dark ftco-section" style="padding: 20px 0 0 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">

                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        &copy;<script>
                            document.write(new Date().getFullYear());
                        </script>  <a href="{{ config('setup.urlcreator') }}" target="_blank">{{ config('setup.creator') }}</a>
                        <!--Creator Template by colorlib.com-->
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </footer>



    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" /></svg></div>



    <script src="{{asset('vendor/landing/js/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/popper.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('vendor/landing/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/aos.js')}}"></script>
    <script src="{{asset('vendor/landing/js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{asset('vendor/landing/js/scrollax.min.js')}}"></script>

    <script src="{{ asset('vendor/landing/landing.js') }}"></script>

    @stack('js_after')
</body>

</html>
