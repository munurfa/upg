@foreach($childs as $child)
    <tr class="treegrid-{{$child->id}} treegrid-parent-{{$dataParent}}">
        <td>{{$child->name}}</td>
        <td>{{$child->keterangan}}</td>
        <td>
            {!! \Orchid\Screen\Actions\DropDown::make()
                ->icon('options-vertical')
                ->list([

                    \Orchid\Screen\Actions\Link::make(__('Edit'))
                        ->route('platform.satker.edit', $child->id)
                        ->icon('pencil'),

                    \Orchid\Screen\Actions\Button::make(__('Delete'))
                        ->icon('trash')
                        ->method('remove')
                        ->confirm('Data akan dihapus, mohon periksa kembali.')
                        ->parameters([
                            'id' => $child->id,
                        ]),
                ]) !!}
        </td>
    </tr>
    @if(count($child->children))
        @include('partials.treetablechild',['childs' => $child->children, 'dataParent' => $child->id])
    @endif
    
@endforeach