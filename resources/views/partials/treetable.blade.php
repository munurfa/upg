<div class="bg-white rounded shadow-sm mb-3 p-4">
   
    {{-- <div class="col p-0 px-3">
        <legend class="text-black">
            <span class="text-danger">*</span>Mohon refresh/reload halaman
        </legend>
    </div> --}}
    @if(!$tree->isEmpty())
    <div class="table-responsive">

        <table class="tree table table-hover table-bordered">
            <tbody>
                <tr>
                    <th>Nama</th>
                    <th>Keterangan</th>
                    <th>Action</th>
                </tr>
                @foreach($tree as $value)
                    <tr class="treegrid-{{$value->id}}">
                        <td>{{$value->name}}</td>
                        <td>{{$value->keterangan}}</td>
                        <td>
                            {!! \Orchid\Screen\Actions\DropDown::make()
                                ->icon('options-vertical')
                                ->list([
    
                                    \Orchid\Screen\Actions\Link::make(__('Edit'))
                                        ->route('platform.satker.edit', $value->id)
                                        ->icon('pencil'),
    
                                    \Orchid\Screen\Actions\Button::make(__('Delete'))
                                        ->icon('trash')
                                        ->method('remove')
                                        ->confirm('Data akan dihapus, mohon periksa kembali.')
                                        ->parameters([
                                            'id' => $value->id,
                                        ]),
                                ]) !!}
                        </td>
                    </tr>
    
                    @if(count($value->children))
                        @include('partials.treetablechild',['childs' => $value->children, 'dataParent' => $value->id ])
                    @endif 
                @endforeach
            </tbody>
        </table>

    </div>
    @endif
@if($tree->isEmpty())
<div class="text-center py-5 w-100">
    <h3 class="font-weight-light">
        <x-orchid-icon path="table" class="block m-b"/>

        {!!  __('There are no records in this view') !!}
    </h3>

</div>
@endif

</div>

@push('stylesheets')
    <link rel="stylesheet" href="{{asset('vendor/jquery-treegrid/css/jquery.treegrid.css')}}">
@endpush

@push('scripts')
    <script src="{{ asset('vendor/jquery-treegrid/js/jquery.treegrid.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.tree').treegrid();
        });
    </script>
@endpush