@extends('platform::auth')
@section('title',__('Verifikasi Email'))

@section('content')
    <h1 class="h4 text-black mb-4">{{__('Akun Belum Terverifikasi')}}</h1>
    <form class="m-t-md"
          role="form"
          method="POST"
          data-controller="layouts--form"
          data-action="layouts--form#submit"
          data-layouts--form-button-animate="#button-login"
          data-layouts--form-button-text="{{ __('Loading...') }}"
          action="{{ route('verification.send') }}">
        @csrf

        <div class="form-group v-center">
            <span class="thumb-sm avatar mr-3">
                <img src="{{ auth()->user()->presenter()->image() }}" class="b bg-light" alt="test">
            </span>
            <span style="width:125px;font-size: 0.85em;">
                <span class="text-ellipsis">{{ auth()->user()->email }}</span>
                <span class="text-muted d-block text-ellipsis">{{ auth()->user()->presenter()->subTitle() }}</span>
            </span>
        </div>
        
        @error('email')
            <span class="d-block invalid-feedback text-danger">
                    {{ $errors->first('email') }}
            </span>
        @enderror
       
        <p class="small">
            {{__('Silahkan periksa email Anda untuk melakukan verifikasi. Pastikan saat melakukan aktivasi anda tidak melakukan logout akun.')}}
        </p>
        
        <div class="row">
            <p class="form-group col-md-6 col-xs-12 small">
                {!!__('Bila Anda tidak menerima email verifikasi, silahkan klik tombol <b>KIRIM
                    ULANG</b>.')!!}
            </p>
            <div class="form-group col-md-6 col-xs-12">
                <button id="button-login" type="submit" class="btn btn-default btn-block" tabindex="2">
                    <x-orchid-icon path="login" class=" text-xs mr-2"/> {{__('Kirim Ulang')}}
                </button>
               
            </div>
        </div>
        
    </form>
    <a href="{{ route('platform.logout') }}"
        class="btn btn-default btn-block"
        data-controller="layouts--form"
        data-action="layouts--form#submitByForm"
        data-layouts--form-id="logout-form"
        dusk="logout-button">
            <x-orchid-icon path="logout" class="mr-2"/>

            <span>{{ __('Sign out') }}</span>
    </a>
    <form id="logout-form"
        class="hidden"
        action="{{ route('platform.logout') }}"
        method="POST"
        data-controller="layouts--form"
        data-action="layouts--form#submit"
    >
        @csrf
    </form>
@endsection
