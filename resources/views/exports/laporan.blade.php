<table border="1">
    <thead>
    <tr>
        <th>No</th>
        <th>Tanggal Laporan</th>
        <th>No Register</th>
    </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $d)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->created_at->isoFormat('DD-MM-YYYY') }}</td>
                <td>{{ $d->noreg }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
